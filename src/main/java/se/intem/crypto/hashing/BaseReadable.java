package se.intem.crypto.hashing;

/**
 * A class of easily readable and typeable characters which should not be confused with each other.
 */
public class BaseReadable implements NumberEncoder {
    // l and I are similar in Arial (also 1). O and 0 are similar.
    // i, l and o are removed.
    // V and Y are similar in some fonts.
    /*
     * Do not include characters such as ?%+ since they need to be coded in a GET-request
     */
    public static final char[] CHARS = "23456789abcdefghjkmnpqrstuwxz".toCharArray();
    public static final int ZZ = CHARS.length * CHARS.length - 1;

    public String encode(long number) {
        char[] buffer = new char[20];
        int index = 0;
        do {
            buffer[index++] = CHARS[(int) (number % CHARS.length)];
            number = number / CHARS.length;
        } while (number > 0);
        return new String(buffer, 0, index);
    }

}
