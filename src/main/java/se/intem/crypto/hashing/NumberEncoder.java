package se.intem.crypto.hashing;

public interface NumberEncoder {
    String encode(long number);
}
