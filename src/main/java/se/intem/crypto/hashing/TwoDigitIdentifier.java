package se.intem.crypto.hashing;

import com.google.common.base.Strings;

public class TwoDigitIdentifier implements NumberEncoder {

    @Override
    public String encode(final long number) {
        return Strings.padStart(Long.toString(number), 2, '0');
    }

}
