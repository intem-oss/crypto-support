package se.intem.crypto.password;

import java.security.SecureRandom;
import java.util.regex.Pattern;
import se.intem.crypto.hashing.BaseReadable;

/**
 * Slumpar fram lösenord.
 *
 * @author Jan-Mikael Bergqvist
 * @author Diana Henriksson
 */
public final class PasswordGenerator {

    private String chars;
    private static final SecureRandom random = new SecureRandom();
    private int length;

    /**
     * Konstruktorn är private, då klassen endast innehåller statiska metoder.
     */
    private PasswordGenerator(final int length) {
        this.chars = new String(BaseReadable.CHARS);
        this.length = length;

    }

    private char randomCharacter() {
        char character = this.chars.charAt(random.nextInt(this.chars.length()));
        return character;
    }

    private String generatePassword() {
        StringBuilder result = new StringBuilder();

        char last = ' ';
        char c;

        for (int i = 0; i < this.length; i++) {

            /* Avoid repeating characters in password. */
            do {
                c = randomCharacter();
            } while (c == last);

            result.append(c);
            last = c;
        }

        return result.toString();

    }

    /**
     * Slumpar fram ett lösenord med den angivna längden.
     */
    public static String generatePassword(final int length) {
        return new PasswordGenerator(length).generatePassword();
    }

    public static String generateUppercase(int length) {

        String generated = new PasswordGenerator(length)
            .generatePassword()
            .toUpperCase();

        /* Password must contain at least one digit */
        return Pattern.compile("[0-9]").matcher(generated).find() ? generated : generateUppercase(length);
    }
}
