package se.intem.crypto.password

import com.google.common.base.CharMatcher
import org.hamcrest.MatcherAssert.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import se.intem.crypto.hashing.BaseReadable
import se.intem.crypto.password.PasswordGenerator

class PasswordGeneratorTest {

    @Test
    fun testGeneratePassword() {
        val length = 8
        val password = PasswordGenerator.generatePassword(length)
        assertNotNull(password)
        assertEquals(password.length, length)

        assertThat(
            "$password should not contain illegal characters",
            CharMatcher.anyOf(String(BaseReadable.CHARS)).matchesAllOf(password)
        )
    }

    @Test
    fun testGenerateUpperCaseCode() {
        val length = 4
        val code = PasswordGenerator.generateUppercase(length)
        assertNotNull(code)
        assertEquals(code.length, length)

        assertThat(
            "$code should not contain illegal characters",
            CharMatcher.anyOf(String(BaseReadable.CHARS).uppercase()).matchesAllOf(code)
        )
    }
}
