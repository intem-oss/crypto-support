package se.intem.crypto.hashing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.intem.crypto.hashing.BaseReadable;

import static org.junit.jupiter.api.Assertions.*;

class BaseReadableTest {

    private BaseReadable encoder;

    @BeforeEach
    void setup() {
        this.encoder = new BaseReadable();
    }

    @Test
    void test_encode() {
        assertEquals("2", encoder.encode(0));
        assertEquals("zz", encoder.encode(29 * 29 - 1));
    }

    @Test
    void max_value() {
        assertEquals("zz", encoder.encode(BaseReadable.ZZ));
    }
}
