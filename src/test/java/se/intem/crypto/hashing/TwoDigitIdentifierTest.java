package se.intem.crypto.hashing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.intem.crypto.hashing.TwoDigitIdentifier;

import static org.junit.jupiter.api.Assertions.*;

class TwoDigitIdentifierTest {

    private TwoDigitIdentifier encoder;

    @BeforeEach
    void setup() {
        this.encoder = new TwoDigitIdentifier();
    }

    @Test
    void should_pad_few_characters() {
        assertEquals("01", encoder.encode(1));
    }

    @Test
    void no_padding_for_request_char_count() {
        assertEquals("78", encoder.encode(78));
    }

    @Test
    void should_support_larger_numbers() {
        assertEquals("240", encoder.encode(240));
    }

    @Test
    void max_value() {
        assertEquals("99", encoder.encode(99));
    }

}
